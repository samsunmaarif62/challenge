const readline = require("readline");
const interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function input(question) {
  return new Promise((resolve) => {
    interface.question(question, (data) => {
      resolve(data);
    });
  });
}

class Kelas{
    constructor(nilai) {
        this.nilai = nilai
    }

    urutData(){
        for (let i = this.nilai.length - 1; i >= 0; i--) {
            for (let j = 1; j <= i; j++) {
                if (this.nilai[j - 1] > this.nilai[j]) {
                    let temp = this.nilai[j - 1];
                    this.nilai[j - 1] = this.nilai[j];
                    this.nilai[j] = temp;
                }
            }
        }
        return this.nilai;
    }

    cariMax(){
        const nilaiUrut = this.urutData();
        return nilaiUrut[nilaiUrut.length - 1];
    }

    cariMin(){
        const nilaiUrut = this.urutData();
        return nilaiUrut[0];
    }

    cariRata(){
        let jumlah = 0;
        const banyak = this.nilai.length; 
        for(let i = 0;i<banyak;i++){
            jumlah+=this.nilai[i]
        }
        return jumlah/banyak;
    }

    cekLulus() {
        const dataKelulusan = [];
        let jumLahLulus = 0;
        let jumLahTidakLulus = 0;
        this.nilai.filter((element) => {
          if (element >= 60) {
            jumLahLulus++;
          } else {
            jumLahTidakLulus++;
          }
        });
        dataKelulusan.push(jumLahLulus);
        dataKelulusan.push(jumLahTidakLulus);
        return dataKelulusan;
    }

    ubahArrayKeString(){
        const dataUrut = this.urutData()
        let dataString = "";
        for(let i = 0; i < dataUrut.length;i++){
            if(i==dataUrut.length-1){
                dataString+=` ${dataUrut[i]}`
            }else if(i==0){
                dataString+= `${dataUrut[i]},`
            }else{
                dataString+=` ${dataUrut[i]},`
            }
        }
        return dataString;
    }

    cekLulus() {
        const dataKelulusan = [];
        let jumLahLulus = 0;
        let jumLahTidakLulus = 0;
        this.nilai.forEach((element) => {
          if (element >= 60) {
            jumLahLulus++;
          } else {
            jumLahTidakLulus++;
          }
        });
        dataKelulusan.push(jumLahLulus);
        dataKelulusan.push(jumLahTidakLulus);
        return dataKelulusan;
    }

    cariNilai(){
        let hasil =["Nilai tidak ditemukan","Nilai tidak ditemukan"];
        const nilai90 = 90;
        const nilai100 = 100;
        this.nilai.forEach(element => {
            if(element==nilai90){
                hasil[0]=element
            }
            if(element==nilai100){
                hasil[1]=element
            }
        });
        return hasil;
    }
}

async function main(){
    let ulang = true;
    let nilai=[];
    while(ulang){
        let data = await input("Masukkan nilai: ");
        if(data=="q"){
            break;
        }else{
            data = +data
            if(isNaN(data)){
                console.log("Nilai tidak valid");
            }else{
                nilai.push(data);
            }
        }
    }
    if(nilai.length == 0){
        console.log('Nilai tidak bisa diproses!')
    }else{
        const KelasA = new Kelas(nilai);
        console.log(`
Nilai tertinggi              : ${KelasA.cariMax()}
Nilai terendah               : ${KelasA.cariMin()}  
Rata - Rata                  : ${KelasA.cariRata()}  
Urutan                       : ${KelasA.ubahArrayKeString()}
Jumlah siswa lulus           : ${(KelasA.cekLulus())[0]}
jumlah siswa tidak lulus     : ${(KelasA.cekLulus())[1]}  
Siswa nilai 90 dan nilai 100 : ${(KelasA.cariNilai())[0]}, ${(KelasA.cariNilai())[1]} 

    `);
        
    }
    interface.close();


}
main()