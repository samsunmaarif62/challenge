--DDL

--BUAT DATABASE
-- Perintah Membuat Database
CREATE DATABASE challenge3;

--BUAT TABLE
-- Perintah Membuat Tabel user_game
CREATE TABLE user_game (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL
);

-- Perintah Membuat Tabel user_game_biodata
CREATE TABLE user_game_biodata (
    id BIGSERIAL PRIMARY KEY,
    bio TEXT,
    id_user_game INT NOT NULL
);

-- Perintah Membuat Tabel user_game_history
CREATE TABLE user_game_history (
    id BIGSERIAL PRIMARY KEY,
    character VARCHAR(50) NOT NULL,
    match_result VARCHAR(50) NOT NULL,
    id_user_game INT NOT NULL
);


-- DML
-- INSERT DATA
-- Masukkan data ke tabel user_game
INSERT INTO user_game (username, password) 
VALUES
    ('samsun123', 'abcd123'),
    ('maarif123', 'qwerty123'),
    ('kipasangin12', 'kabel12'),
    ('mieayam99', 'enakkk123'),
    ('nutrisar1', 'seger123');

-- Masukkan data ke tabel user_game_biodata
INSERT INTO user_game_biodata (bio, id_user_game) 
VALUES
    ('Shinning like sun', 1),
    ('Stay cool', 2),
    ('will you fall down?', 3),
    ('Minat DM', 4),
    ('Winning is like breathing', 5);

-- Masukkan data ke tabel user_game_history
INSERT INTO user_game_history (character, match_result,  id_user_game) 
VALUES
    ('Karina', 'win', 1),
    ('F-Saber', 'lose', 1),
    ('Robin', 'draw', 1),
    ('Selena', 'Win', 2),
    ('Park Il Pyo', 'Win', 2),
    ('Gumiho', 'draw', 3),
    ('Hazama', 'lose', 4),
    ('Nang In', 'Win', 5);

-- SELECT DATA
SELECT * FROM user_game;
SELECT * FROM user_game_biodata;
SELECT * FROM user_game_history;

-- UPDATE DATA
-- ganti bio dengan id_user_game = 4
UPDATE user_game_biodata 
SET bio = 'Diam tolah toleh, Bergerah rata kabeh'
WHERE id_user_game = 4;
-- liat hasil update
SELECT * FROM user_game_biodata WHERE id_user_game = 4;;

--DELETE DATA
-- hapus history game dengan id_user_game = 5
DELETE FROM user_game_history 
WHERE id_user_game = 5;
-- liat hasil delete
SELECT * FROM user_game_history;
