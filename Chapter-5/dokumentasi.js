const { Collection, Item, Header } = require("postman-collection");
const fs = require("fs");

// create collection
const postmanCollection = new Collection({
  info: {
    name: "Dokumentasi RESTful API Challenge Chapter-5",
  },
  item: [],
});

//User
const register = new Item({
  name: "Register",
  request: {
    header: {
        "Content-type": "application/json",
    },
    url: "http://localhost:3000/auth/register",
    method: "POST",
    body: {
      mode: "raw",
      raw: JSON.stringify({
        name: "User ke-100",
        username: "user100",
        password: "password123"
      }),
    },
    auth: null,
  },
});
const login = new Item({
  name: "Login",
  request: {
    header: {
        "Content-type": "application/json",
    },
    url: "http://localhost:3000/auth/login",
    method: "POST",
    body: {
      mode: "raw",
      raw: JSON.stringify({
        username: "user100",
        password: "password123"
      }),
    },
    auth: null,
  },
});
const changepassword = new Item({
  name: "Change Password",
  request: {
    header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
    url: "http://localhost:3000/auth/changepassword",
    method: "PUT",
    body: {
      mode: "raw",
      raw: JSON.stringify({
        oldPassword : "password123",
        newPassword : "password789",
        confirmNewPassword : "password789"
      }),
    },
    auth: null,
  },
});
const changeprofile = new Item({
  name: "Change Profile",
  request: {
    header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
    url: "http://localhost:3000/auth/changeprofile",
    method: "PUT",
    body: {
      mode: "raw",
      raw: JSON.stringify({
        name:"user100 update",
	      username : "user100-update"
      }),
    },
    auth: null,
  },
});
const deleteaccount = new Item({
    name: "Delete Akun",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/auth/deleteAccount",
      method: "DELETE",
      auth: null,
    },
  });
//Bio
const addbio = new Item({
    name: "Add Biodata",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/bio/addbio",
      method: "POST",
      body: {
        mode: "raw",
        raw: JSON.stringify({
            biodata: "user fanny 100% winrate"
        }),
      },
      auth: null,
    },
  });
const showbio = new Item({
    name: "Show Biodata",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/bio/showbio",
      method: "GET",
      auth: null,
    },
  });
const updatebio = new Item({
    name: "Update Biodata",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/bio/updatebio",
      method: "PUT",
      body: {
        mode: "raw",
        raw: JSON.stringify({
            biodata:"ini bio baru"
        }),
      },
      auth: null,
    },
});
const deletebio = new Item({
    name: "Delete Biodata",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/bio/deletebio",
      method: "DELETE",
      auth: null,
    },
  });
  
//History
const addhistory = new Item({
    name: "Add History",
    request: {
    header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
    url: "http://localhost:3000/history/addhistory",
    method: "POST",
    body: {
        mode: "raw",
        raw: JSON.stringify({
            character:"link",
            match_result:"lose"
        }),
    },
    auth: null,
    },
});
const showhistory = new Item({
    name: "Show History",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/history/showhistory",
      method: "GET",
      auth: null,
    },
});
const updatehistory = new Item({
    name: "Update History",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000//history/updatehistory",
      method: "PUT",
      body: {
        mode: "raw",
        raw: JSON.stringify({
            id:1,
            character :"link",
            match_result :"win"
        }),
      },
      auth: null,
    },
  });
const deletehistory = new Item({
    name: "Delete History",
    request: {
      header: {
        "Content-type": "application/json",
        "Authorization" : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywibmFtZSI6IlVzZXIga2UtMTAwIiwidXNlcm5hbWUiOiJ1c2VyMTAwIiwiaWF0IjoxNjY1MTExNjg3fQ.-824irBhkyz9JMiqY-dnfGqjVP-OcVnyMXMtDan8jcM"
    },
      url: "http://localhost:3000/history/deletehistory",
      method: "DELETE",
      body: {
        mode: "raw",
        raw: JSON.stringify({
            id:1
        }),
      },
      auth: null,
    },
  });

postmanCollection.items.add(register);
postmanCollection.items.add(login);
postmanCollection.items.add(changepassword);
postmanCollection.items.add(changeprofile);
postmanCollection.items.add(deleteaccount);

postmanCollection.items.add(addbio);
postmanCollection.items.add(showbio);
postmanCollection.items.add(updatebio);
postmanCollection.items.add(deletebio);

postmanCollection.items.add(addhistory);
postmanCollection.items.add(showhistory);
postmanCollection.items.add(updatehistory);
postmanCollection.items.add(deletehistory);

// convert to json
const collectionJSON = postmanCollection.toJSON();

//export to file
fs.writeFile("./collection.json", JSON.stringify(collectionJSON), (err) => {
  if (err) console.log(err);
  console.log("file saved");
});
