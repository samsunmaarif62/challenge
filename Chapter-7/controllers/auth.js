const { user_game } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const roles = require('../utils/roles');
const googleOauth2 = require('../utils/oauth2/google');
const facebookOauth2 = require('../utils/oauth2/facebook');

const {
    JWT_SIGNATURE_KEY
} = process.env;

module.exports = {
    register: async (req, res, next) => {
        try {
            const { name, username, password, role = roles.user } = req.body;

            const existUser = await user_game.findOne({ where: { username: username } });
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: 'username already used!'
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);
            const user = await user_game.create({
                name,
                username,
                password: encryptedPassword,
                role
            });

            return res.status(201).json({
                status: true,
                message: 'success',
                data: {
                    name: user.name,
                    username: user.username,
                    role:user.role
                }
            });
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const { username, password } = req.body;

            const user = await user_game.findOne({ where: { username: username } });
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'username or password doesn\'t match!'
                });
            }

            const correct = await bcrypt.compare(password, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: 'username or password doesn\'t match!'
                });
            }

            // generate token
            payload = {
                id: user.id,
                name: user.name,
                username: user.username,
                role: user.role
            };
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    token: token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    
 
    changePassword: async (req, res, next) => {
        try {
            const { id_user_game, oldPassword, newPassword, confirmNewPassword } = req.body;
            
            if (newPassword !== confirmNewPassword) {
                return res.status(422).json({
                    status: false,
                    message: 'new password and confirm new password doesnt match!'
                });
            }

            const user = await user_game.findOne({ where: { id: id_user_game} });
            if (!user) return res.status(404).json({ success: false, message: 'User not found!' });

            const correct = await bcrypt.compare(oldPassword, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: 'old password does not match!'
                });
            }

            const encryptedPassword = await bcrypt.hash(newPassword, 10);
            const updatedUser = await user_game.update({
                password: encryptedPassword
            }, {
                where: {
                    id: user.id
                }
            });

            return res.status(200).json({
                status: true,
                message: 'success',
                data: updatedUser
            });

        } catch (err) {
            next(err);
        }
    },

    changeProfile: async (req, res, next) => {
        try {
            let { id_user_game, name, username } = req.body;
            
            const user = await user_game.findOne({ where: { id: id_user_game } });
            if (!user) return res.status(404).json({ success: false, message: 'User not found!' });

            if (!name){
                name=user.name
            }
            if (!username){
                username=user.username
            }
            const updatedUser = await user_game.update({
                name: name,
                username: username
            }, {
                where: {
                    id: user.id
                }
            });

            return res.status(200).json({
                status: true,
                message: 'update success',
                data: updatedUser
            });

        } catch (err) {
            next(err);
        }
    },

    deleteAccount: async (req, res, next) => {
        try {
            const { id } = req.user;
            const user = await user_game.findOne({ where: { id: id } });
            if (!user) {
                return res.status(404).json({
                status: false,
                message: "user not found!",
                });
            }
            await user_game.destroy({ where: { id: id } });
            return res.status(200).json({
                status: true,
                message: "success",
            });
        }catch (error) {
            next(error);
        }
    },

    //fb google login

    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = googleOauth2.generateAuthURL();
                return res.redirect(url);
            }

            // get token
            await googleOauth2.setCredentials(code);

            // get data user
            const { data } = await googleOauth2.getUserData();

            //karena di database ini tidak memakai email melainkan username
            //jadi buat username dari email menggunakan split
            // email : samsun@gmail
            //hasil username : samsun
            let getemail = data.email
            let hasilSplit = getemail.split('@')
            let getusername = hasilSplit[0]
            // check apakah user username ada di database
            let userExist = await user_game.findOne({ where: { username: getusername } });

            // if !ada -> simpan data user
            if (!userExist) {
                userExist = await user_game.create({
                    name: data.name,
                    username: getusername,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: userExist.id,
                name: userExist.name,
                username: userExist.username,
                role: userExist.role,
                
            };
            const token = jwt.sign(payload, process.env.JWT_SIGNATURE_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: userExist.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    facebook: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = facebookOauth2.generateAuthURL();
                return res.redirect(url);
            }
            // return res.send('berhasil')
            // acces_token
            const access_token = await facebookOauth2.getAccessToken(code);

            // return res.json(access_token)
            // // get user info
            const userInfo = await facebookOauth2.getUserInfo(access_token);
            // return res.send(userInfo)
            // //buat username
            let getemail = userInfo.email
            let hasilSplit = getemail.split('@')
            let getusername = hasilSplit[0]

            // // check apakah user email ada di database
            let userExist = await user_game.findOne({ where: { username: getusername } });

            // if !ada -> simpan data user
            if (!userExist) {
                userExist = await user_game.create({
                    name: [userInfo.first_name, userInfo.Last_name].join(' '),
                    username: getusername,
                    role: roles.user
                });
            }

            // generate token
            const payload = {
                id: userExist.id,
                name: userExist.name,
                username: userExist.username,
                role: userExist.role,
            };
            const token = jwt.sign(payload, process.env.JWT_SIGNATURE_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    user_id: userExist.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    }
    
}