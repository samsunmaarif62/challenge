const auth = require('./auth')
const bio = require('./bio')
const history = require('./history')
const media = require('./media')

module.exports = {auth, bio, history, media }