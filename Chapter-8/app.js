require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
const app = express();
const methodOverride = require('method-override');

const {
    HTTP_PORT = 3000,
    VAPID_PUBLIC_KEY,
    VAPID_PRIVATE_KEY,
    VAPID_SUBJECT
} = process.env;

app.use(express.json());
app.use(morgan('dev'));
const cors = require('cors');
const path = require('path');
const webpush = require('web-push');
app.use(cors());
webpush.setVapidDetails(VAPID_SUBJECT, VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);
app.set('view engine', 'ejs');
app.use(methodOverride('_method'));
app.use(express.urlencoded({ extended: true }));
app.use(router)
// app.use(express.static('public/videos'));
app.use('/public/videos',express.static('public/videos'));
// app.use(express.static(path.join(__dirname, 'client')));
app.use('/client',express.static('client'));

app.post('/notification/subscribe', async (req, res) => {
    try {
        // const subscriptions = require('./subscriptions.json');
        const subscription = req.body;
        // console.log('ini sub',subscription)
        // console.log('ini subs',subscriptions)

        // save subscription data
        // subscriptions.push(subscription);
        // fs.writeFile('./subscriptions.json', JSON.stringify(subscriptions), (err) => {
        //     console.log = (err);
        // });

        const payload = JSON.stringify({
            title: 'Selamat bergabung dalam game !',
            body: 'Jangan lupa beli skin yang banyak agar kami kaya.'
        });
        // console.log("subs"+subscription)
        webpush.sendNotification(subscription, payload)
            .then(result => console.log(result))
            .catch(e => console.log(e.stack));

        res.status(200).json({ 'success': true });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            status: false,
            message: err.message
        });
    }
});
// 404 handler
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'Are you lost?'
    });
});

// 500 handler
app.use((err, req, res, next) => {
    return res.status(500).json({
        status: false,
        message: err.message
    });
});

app.listen(HTTP_PORT, () => console.log('listening on port', HTTP_PORT));
