
const { media } = require('../models');

module.exports = {
    simpan: async (req, res, next) => {
        try {
            const file = req.file
            // console.log(file)
            
            const buatmedia = await media.create({
                filename: req.file.filename,
                fileurl: req.file.path
            })

            return res.status(200).json({
                status: true,
                message: 'upload video single success',
                data: {
                    filename: req.file.name,
                    fileurl: req.file.path
                }
            });
        } catch (err) {
            next(err);
        }
    },

    show: async (req, res, next) => {
        try {
            const { filename } = req.body;
            // console.log(filename)
            const mediaexist = await media.findOne({ where: { filename: filename } });  
            if (mediaexist) {
                return res.status(200).json({
                    status:true,
                    message: 'show video succes',
                    url:'http://localhost:3000/public/videos/'+filename
                })
                // res.redirect('http://localhost:3000/public/videos/'+filename)
            }else{
                return res.status(404).json({
                    status:false,
                    message: 'video not found'
                })
            }
        } catch (err) {
            next(err);
        }
    },
};