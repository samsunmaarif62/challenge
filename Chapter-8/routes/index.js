const express = require('express');
const router = express.Router();
const c = require('../controllers');
const mid = require('../helpers/middleware');
const storage = require('../utils/storage');



//=================Chapter 8=========================================================
router.get('/auth/register', c.auth.signUp);
router.post('/auth/register', c.auth.register);


router.get('/auth/forgot-password', c.auth.forgotPasswordView);
router.post('/auth/forgot-password', c.auth.forgotPassword);

router.get('/auth/reset-password', c.auth.resetPasswordView);
router.post('/auth/reset-password', c.auth.resetPassword);

router.post('/auth/payment-confirmation', mid.mustLogin, c.auth.confirmPayment);
//==================================================================================

//Fitur untuk tabel user_game : daftar, login, ganti password, ganti name dan username
//dan hapus akun
router.post('/auth/login', c.auth.login);
router.put('/auth/changepassword', mid.mustAdmin, c.auth.changePassword);
router.put('/auth/changeprofile', mid.mustAdmin, c.auth.changeProfile);
router.delete("/auth/deleteAccount", mid.mustAdmin, c.auth.deleteAccount);

//Fitur untuk tabel user_game_biodata : tampilkan data biodata, menambahkan biodata, mengubah biodata, 
//dan menghapus biodata
router.post("/bio/addbio", mid.mustAdmin, c.bio.add);
router.get("/bio/showbio", mid.mustLogin, c.bio.show);
router.put("/bio/updatebio", mid.mustAdmin, c.bio.update);
router.delete("/bio/deletebio", mid.mustAdmin, c.bio.delete);

//Fitur untuk tabel user_game_biodata : tampilkan semua data history dari user, menambahkan history, mengubah history, 
//dan menghapus history
router.post("/history/addhistory", mid.mustAdmin, c.history.add);
router.get("/history/showhistory", mid.mustLogin, c.history.showAll);
router.put("/history/updatehistory", mid.mustAdmin, c.history.update);
router.delete("/history/deletehistory", mid.mustAdmin, c.history.delete);

//Login dengan FB dan Google
router.get('/auth/login/google', c.auth.google);
router.get('/auth/login/facebook', c.auth.facebook);


//upload dan show video
router.post('/upload/single', mid.mustAdmin, storage.video.single('media'), c.media.simpan);
router.get('/showvideo', mid.mustLogin, c.media.show);

// router.post('/upload/multiple', mid.mustAdmin, storage.video.array('media'), (req, res) => {
//     const files = [];
//     req.files.forEach(file => {
//         const imageUrl = file.path;

//         files.push(imageUrl);
//     }); 
//     return res.status(200).json({
//         status: true,
//         message: 'upload video multiple success',
//         data: {
//             path : files
//         }
//     });
// });

module.exports = router;
