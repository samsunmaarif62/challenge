const multer = require('multer');
const path = require('path');
const { media } = require('../models');



const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './public/videos');
    },

    // generete unique filename
    filename: (req, file, callback) => {
        const namaFile = Date.now() + path.extname(file.originalname);
        filename = namaFile;
        callback(null, namaFile);
    }
});
function simpanmedia(filename, fileurl){
    media.create({
        filename: filename,
        fileurl: fileurl
    })
}

module.exports = {
    video: multer({
        storage: storage,
        // add file filter
        fileFilter: (req, file, callback) => {
            console.log(file.mimetype)
            if (file.mimetype == 'video/mp4' || file.mimetype == 'video/mkv' || file.mimetype == 'video/3gp') {
                //simpan database media
                console.log(file)
                callback(null, true);
            } else {
                const err = new Error('only mp4, 3gp, and mkv allowed to upload!');
                return callback(err, false);
            }
        },

        // error handling
        onError: (err, next) => {
            next(err);
        }
    })

};